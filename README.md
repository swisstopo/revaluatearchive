# Revaluate swisstopo's geological archive

OCR, text classification and object detection pipelines to revaluate scanned geological paper reports stored in the 
geological archive of swisstopo (Swiss Federal Office of Topography). 

Web application for interacting with the available document metadata from swisstopo with the newly generated
predictions from text classification and object detection.

### Overview

![Workflow](workflow_readme.png)

Detailed information on the two pipelines as well as for the web application can be found within the respective readme:
* [Text classification](src/document_classification/README.md)
* [Object detection](src/object_detection/README.md)
* [Web application](src/web_app/README.md)


### Citation recommendation
> Morgenthaler, J., Frefel, D., Meier, J., Oesterling, N., Perren, G., Heuberger, S. (2022). Revalue Geoscientific Data 
Utilising Deep Learning, *Swiss Bulletin for Applied Geology 27/1, 45-54.*




