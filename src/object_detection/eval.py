import os
import sys
import cv2
import json
import random
from sklearn.model_selection import train_test_split
from collections import Counter
from detectron2.utils.logger import setup_logger
setup_logger()
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog, build_detection_test_loader
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.structures import BoxMode 

from detectron2.structures.boxes import pairwise_iou, Boxes
import numpy as np
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from scikitplot.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
 
class Things:
    def __init__(self):
        self.things = {}

    def __getitem__(self, key):
        if key not in self.things:
            self.things[key] = len(self.things)
        return self.things[key]


def evaluate_boxes(true_boxes, true_labels, pred_boxes, pred_labels, pred_scores, class_labels, iou_tresh=0.5, score_tresh=0.75):
    true_classes = []
    pred_classes = []
    pred_open = list(range(len(pred_boxes)))
    
    ious = pairwise_iou(true_boxes, pred_boxes).numpy()
    
    for i, t in enumerate(true_labels):
        iou = ious[i]
        true_classes.append(class_labels[t])
        iou[pred_scores < score_tresh] = 0
        
        if len(iou) > 0 and iou.max() >= iou_tresh:
            max_i = iou.argmax()
            if max_i in pred_open:
                pred_open.remove(max_i)
            pred_classes.append(class_labels[pred_labels[max_i]])
        else:      
            pred_classes.append('null')

    for i in pred_open:
        if pred_scores[i] >= score_tresh:
            true_classes.append('null')
            pred_classes.append(class_labels[pred_labels[i]])
        
    return true_classes, pred_classes
    
    
def get_data_dict_list():
    things = Things()
    pages = {}
    data = {}
    
    with open("bdata/annotations_export.txt", 'r') as f:
        s = f.read()
        s = s.replace("\'", "\"")
        data = json.loads(s)
        
    for page in data:
        page_source = page['_source']
        im = cv2.imread('bdata/' + page_source['path'].split('/')[-1])
        h = im.shape[0]
        w = im.shape[1]

        page_dict = {
            'image_id': page['_id'],
            'file_name': 'bdata/' + page_source['path'].split('/')[-1],
            'width': w,
            'height': h,
            'annotations': []
        }
        pages[page['_id']] = page_dict
    
    for annotation in data:
        source = annotation['_source']
        annotation_dict = {
            'bbox': [float(c) for c in source['bbox']],
            'bbox_mode': BoxMode.XYXY_ABS,
            'category_id': things[source['category']],
            'category': source['category']
        }
        pages[annotation['_id']]['annotations'].append(annotation_dict)
    dict_list = [page_dict for i, page_dict in pages.items() if len(page_dict['annotations']) > 0]
    
    return dict_list, list(things.things.keys())

def get_train_val_data():
    dict_list, things = get_data_dict_list()
    train_dicts, val_dicts = train_test_split(dict_list, test_size=0.1, random_state=3)

    count_images_with_annotation = lambda dict_list: sum([1 for d in dict_list if len(d['annotations']) > 0])
    count_annotation = lambda dict_list: sum([len(d['annotations']) for d in dict_list])

    print(f'{"":25} | {"All":>5} | {"Train":>5} | {"Test":>5}')
    print('-' * 50)

    print(f'{"# images":<25} | {len(dict_list):>5} | {len(train_dicts):>5} | {len(val_dicts):>5}')
    print(
        f'{"# images with annotations":<25} | {count_images_with_annotation(dict_list):>5} | {count_images_with_annotation(train_dicts):>5} | {count_images_with_annotation(val_dicts):>5}')
    print(
        f'{"# annotations":<25} | {count_annotation(dict_list):>5} | {count_annotation(train_dicts):>5} | {count_annotation(val_dicts):>5}')

    counter_all = Counter([a['category'] for d in dict_list for a in d['annotations']])
    counter_train = Counter([a['category'] for d in train_dicts for a in d['annotations']])
    counter_val = Counter([a['category'] for d in val_dicts for a in d['annotations']])
    print('-' * 50)
    for thing, _ in counter_all.most_common():
        print(f'{thing:25} | {counter_all[thing]:>5} | {counter_train[thing]:>5} | {counter_val[thing]:>5}')

    return train_dicts, val_dicts, things
    

model_path = 'model/object_extraction_model2'
train_dicts, val_dicts, things = get_train_val_data()
DatasetCatalog.register("gaia_dataset_val", lambda: val_dicts)
MetadataCatalog.get("gaia_dataset_val").thing_classes = things

dataset_val = DatasetCatalog.get("gaia_dataset_val")
metadata_val = MetadataCatalog.get("gaia_dataset_val")

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
cfg.MODEL.WEIGHTS = os.path.join(model_path, "model_0134999.pth") 
cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(things)
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5   # set the testing threshold for this model
cfg.MODEL.DEVICE = 'cuda'

predictor = DefaultPredictor(cfg)

evaluator = COCOEvaluator("gaia_dataset_val", ('bbox',), False, output_dir=model_path + '_eval')
val_loader = build_detection_test_loader(cfg, "gaia_dataset_val")
inference_on_dataset(predictor.model, val_loader, evaluator)

saved_classes_scores = {}
for iou_tresh in [0.5, 0.75, 0.95]:
    for score_tresh in [0.5, 0.75, 0.95]:
        all_true_classes = []
        all_pred_classes = []
        all_pred_scores = []

        for data_dict in dataset_val:
            img = cv2.imread(data_dict['file_name'])
            outputs = predictor(img)

            true_boxes = Boxes([a['bbox'] for a in data_dict['annotations']])
            true_labels = np.array([a['category_id'] for a in data_dict['annotations']])

            pred_boxes = outputs['instances'].pred_boxes.to('cpu')
            pred_labels = outputs['instances'].pred_classes.to('cpu').numpy()
            pred_scores = outputs['instances'].scores.to('cpu').numpy()

            true_classes, pred_classes = evaluate_boxes(true_boxes, true_labels, pred_boxes, pred_labels, pred_scores, things, iou_tresh, score_tresh)
            all_true_classes += true_classes
            all_pred_classes += pred_classes
            all_pred_scores +=  pred_scores.tolist()

        saved_classes_scores[(iou_tresh, score_tresh)] = (all_true_classes, all_pred_classes)
		
for i in [0.5, 0.75, 0.95]:
    for j in [0.5, 0.75, 0.95]:
        k = (i, j)
        plot_confusion_matrix(*saved_classes_scores[k], x_tick_rotation=90, hide_zeros=True, title=f'IoU: {k[0]}, Score-Treshold: {k[1]}')
        plt.savefig('output_'+str(i)+'_'+str(j)+'.jpg')