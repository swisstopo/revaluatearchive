import os
import cv2
import argparse
from detectron2.utils.logger import setup_logger

setup_logger()
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg


def parse_args():
    """Use argparse to parse command line arguments and pass it on to the main function."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'imgpath',
        help = 'Path to image to run the model on')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    model_path = 'model/object_extraction_model2'
    im = cv2.imread(args.imgpath)

    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
    cfg.MODEL.WEIGHTS = os.path.join(model_path, "model_0134999.pth")
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 10
    cfg.MODEL.DEVICE = 'cuda'

    predictor = DefaultPredictor(cfg)
    result = predictor(im)

    print(result)
