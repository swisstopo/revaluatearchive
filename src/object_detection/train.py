import os
import copy

import torch
import json

from detectron2.engine import DefaultTrainer
from detectron2.evaluation import COCOEvaluator
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, build_detection_train_loader, DatasetCatalog
from detectron2.data import detection_utils as utils
from detectron2 import model_zoo
import detectron2.data.transforms as T
from detectron2.structures import BoxMode 
import json
import cv2
from sklearn.model_selection import train_test_split
from collections import Counter


#Container class for all annotated objects
class Things:
    def __init__(self):
        self.things = {}

    def __getitem__(self, key):
        if key not in self.things:
            self.things[key] = len(self.things)
        return self.things[key]


#Read in the data
def get_data_dict_list():
    things = Things()
    pages = {}
    data = {}
    
    with open("bdata/annotations_export.txt", 'r') as f:
        s = f.read()
        s = s.replace("\'", "\"")
        data = json.loads(s)
        
    for page in data:
        page_source = page['_source']
        im = cv2.imread('bdata/' + page_source['path'].split('/')[-1])
        h = im.shape[0]
        w = im.shape[1]

        page_dict = {
            'image_id': page['_id'],
            'file_name': 'bdata/' + page_source['path'].split('/')[-1],
            'width': w,
            'height': h,
            'annotations': []
        }
        pages[page['_id']] = page_dict
    
    for annotation in data:
        source = annotation['_source']
        annotation_dict = {
            'bbox': [float(c) for c in source['bbox']],
            'bbox_mode': BoxMode.XYXY_ABS,
            'category_id': things[source['category']],
            'category': source['category']
        }
        pages[annotation['_id']]['annotations'].append(annotation_dict)
    dict_list = [page_dict for i, page_dict in pages.items() if len(page_dict['annotations']) > 0]
    
    return dict_list, list(things.things.keys())


#Process and split training and evaluation data
def get_train_val_data():
    dict_list, things = get_data_dict_list()
    train_dicts, val_dicts = train_test_split(dict_list, test_size=0.1, random_state=3)

    count_images_with_annotation = lambda dict_list: sum([1 for d in dict_list if len(d['annotations']) > 0])
    count_annotation = lambda dict_list: sum([len(d['annotations']) for d in dict_list])

    print(f'{"":25} | {"All":>5} | {"Train":>5} | {"Test":>5}')
    print('-' * 50)

    print(f'{"# images":<25} | {len(dict_list):>5} | {len(train_dicts):>5} | {len(val_dicts):>5}')
    print(
        f'{"# images with annotations":<25} | {count_images_with_annotation(dict_list):>5} | {count_images_with_annotation(train_dicts):>5} | {count_images_with_annotation(val_dicts):>5}')
    print(
        f'{"# annotations":<25} | {count_annotation(dict_list):>5} | {count_annotation(train_dicts):>5} | {count_annotation(val_dicts):>5}')

    counter_all = Counter([a['category'] for d in dict_list for a in d['annotations']])
    counter_train = Counter([a['category'] for d in train_dicts for a in d['annotations']])
    counter_val = Counter([a['category'] for d in val_dicts for a in d['annotations']])
    print('-' * 50)
    for thing, _ in counter_all.most_common():
        print(f'{thing:25} | {counter_all[thing]:>5} | {counter_train[thing]:>5} | {counter_val[thing]:>5}')

    return train_dicts, val_dicts, things


class CocoTrainer(DefaultTrainer):
    augment = False

    #Image processing
    @staticmethod
    def custom_mapper(dataset_dict):
        dataset_dict = copy.deepcopy(dataset_dict)  # it will be modified by code below
        image = utils.read_image(dataset_dict["file_name"], format="BGR")

        transform_list = [T.ResizeShortestEdge(short_edge_length=[640, 672, 704, 736, 768, 800], max_size=1333, sample_style='choice')]
        if CocoTrainer.augment:
            transform_list = [
                                 T.RandomBrightness(0.8, 1.8),
                                 T.RandomContrast(0.6, 1.3),
                                 T.RandomSaturation(0.8, 1.4),
                                 T.RandomLighting(0.7),
                                 T.RandomCrop('relative_range', (0.9, 1)),
                             ] + transform_list
        image, transforms = T.apply_transform_gens(transform_list, image)
        dataset_dict["image"] = torch.as_tensor(image.transpose(2, 0, 1).astype("float32"))

        annos = [utils.transform_instance_annotations(obj, transforms, image.shape[:2]) for obj in
                 dataset_dict.pop("annotations")]
        dataset_dict["instances"] = utils.annotations_to_instances(annos, image.shape[:2])
        dataset_dict["instances"] = utils.filter_empty_instances(dataset_dict["instances"])
        return dataset_dict

    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        if output_folder is None:
            output_folder = cfg.EVAL_DIR
            os.makedirs(output_folder, exist_ok=True)

        return COCOEvaluator(dataset_name, ('bbox',), False, output_folder)

    @classmethod
    def build_train_loader(cls, cfg):
        return build_detection_train_loader(cfg, mapper=CocoTrainer.custom_mapper)


class ObjectExtractionTrainer:
    def __init__(self, things, model_path):
        self.model_path = model_path
        self.things = things
        self.cfg = self._get_train_config()
        os.makedirs(self.cfg.OUTPUT_DIR, exist_ok=True)


    #Training parameters
    def _get_train_config(self):
        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
        cfg.DATASETS.TRAIN = ("gaia_dataset_train",)
        cfg.DATASETS.TEST = ("gaia_dataset_val",)
        cfg.DATALOADER.NUM_WORKERS = 0
        cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml")
        cfg.SOLVER.IMS_PER_BATCH = 1
        cfg.SOLVER.BASE_LR = 0.003
        cfg.SOLVER.WARMUP_ITERS = 1000
        cfg.SOLVER.STEPS = (1000, 1500)
        cfg.SOLVER.GAMMA = 0.05
        cfg.MODEL.PROPOSAL_GENERATOR.MIN_SIZE = 50
        cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 32
        cfg.MODEL.RPN.IOU_THRESHOLDS = [0.2, 0.8]
        cfg.MODEL.RPN.NMS_THRESH = 0.8
        cfg.MODEL.PANOPTIC_FPN.COMBINE.OVERLAP_THRESH = 0.25
        cfg.TEST.EVAL_PERIOD = 100
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(self.things)

        cfg.OUTPUT_DIR = self.model_path
        cfg.EVAL_DIR = cfg.OUTPUT_DIR + '_eval'
        return cfg


    #Training method
    def train(self, train_dicts, val_dicts, iterations, device='cuda'):
        DatasetCatalog.register("gaia_dataset_train", lambda: train_dicts)
        DatasetCatalog.register("gaia_dataset_val", lambda: val_dicts)

        MetadataCatalog.get("gaia_dataset_train").thing_classes = self.things
        MetadataCatalog.get("gaia_dataset_val").thing_classes = self.things

        self.cfg.SOLVER.MAX_ITER = iterations
        self.cfg.MODEL.DEVICE = device

        trainer = CocoTrainer(self.cfg)
        trainer.resume_or_load(resume=False)
        trainer.train()


    #Model saving
    def save(self):
        with open(os.path.join(self.model_path, 'model.cfg'), 'w') as f:
            self.cfg.pop('EVAL_DIR')
            f.write(self.cfg.dump())

        with open(os.path.join(self.model_path, 'metadata.txt'), 'w') as f:
            json.dump(self.things, f)


if __name__ == '__main__':
    train_dicts, val_dicts, things = get_train_val_data()

    model_path = 'model/object_extraction_model2'
    trainer = ObjectExtractionTrainer(things, model_path)
    trainer.train(train_dicts, val_dicts, 200000, 'cuda')
    trainer.save()
