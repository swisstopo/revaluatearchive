## Instance detection

Requirements

- Python >= 3.6
- Pytorch >= 1.5 with matching torchvision [](https://github.com/pytorch/vision/)
- Detectron2: https://github.com/facebookresearch/detectron2

## Data format

Detectron2’s standard dataset dictionary

| Field        | Type      | Description        
|--------------|-----------|----------------- 
| image_id     | str or int| unique identifier
| file_name    |           | full path to the image
| width, height| int       | shape of the image
| annotations  | list[dict]| 

The annotations are dictionaries themselves and contain, for each instance, four keys; bbox (list[float]), bbox_mode (int), category_id (int), and category (string). A detailed description of bbox, bbox_mode, and category_id can be found here [1](https://detectron2.readthedocs.io/tutorials/datasets.html). The key category represents the class of the instance as described above.

## Scripts
### train.py
This script is used to train the object detection model. <br>
Input parameter: labeled data <br>
Output: model files

### eval.py
This script is used to evaluate the object detection model. <br>
Input: labeled data and a trained model <br>
Output: evaluation result

### inference.py
This script is used to run the model on a single (unlabeled) image. <br>
Input: path to unlabeled image <br>
Output: Detected instances <br>
Example output: <br>
{ <br>
'instances': Instances(num_instances=5, image_height=4837, image_width=1974, <br>
            fields=[pred_boxes: Boxes(tensor([[ 265.5120,  693.3475, 1671.7787, 3047.6660], <br>
                    [ 190.4593,  632.4573, 1689.6106, 2896.4426], <br>
                    [ 211.3105,  717.8551, 1772.6077, 2941.9399], <br>
                    [ 191.3304,  774.0904, 1724.2040, 1446.1908], <br>
                    [ 193.7187,  787.7521, 1741.6455, 2870.1770]], device='cuda:0')), <br>
            scores: tensor([0.3622, 0.2460, 0.1347, 0.1167, 0.0742], device='cuda:0'), <br>
            pred_classes: tensor([1, 4, 7, 8, 8], device='cuda:0')]) <br>
} <br>
This means that the model detected 5 object candidates. The location of the objects are given as bounding boxes. The scores represent the probability that a candidate is an acutal object. pred_classes indicates the detected object type. <br>
Object types: ['Grafik', 'Stratigraphisches Profil', 'Geotechnisches Profil', 'Tabelle', 'Bohrprofil', 'Karte', 'Geologisches Profil', 'Bohr-Log', 'Foto', 'Seismisches Profil'] <br>

