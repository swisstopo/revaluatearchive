import os
import numpy as np
import logging
from pathlib import Path
import random
import re
import warnings
import pandas as pd
from shutil import copyfile
import json

from farm.data_handler.data_silo import DataSilo
from farm.data_handler.processor import TextClassificationProcessor
from farm.modeling.optimization import initialize_optimizer
from farm.infer import Inferencer
from farm.modeling.adaptive_model import AdaptiveModel
from farm.modeling.language_model import LanguageModel
from farm.modeling.prediction_head import MultiLabelTextClassificationHead
from farm.modeling.tokenization import Tokenizer
from farm.train import Trainer, EarlyStopping
from farm.eval import Evaluator
from farm.utils import MLFlowLogger, initialize_device_settings
from farm.utils import set_all_seeds
from farm.evaluation.metrics import simple_accuracy, register_metrics
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import confusion_matrix, f1_score

warnings.simplefilter("ignore")
logging.disable(logging.CRITICAL)
pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 20)

# Parameter definitions
data_location = 'Labeling-FHNW-neu/'
data_location2 = 'Ergaenzung-Labeling/'
skip_first = False
skip_second = False
independent = True
n_epochs = 100
lang_model = "bert-base-multilingual-cased"
do_lower_case = False
fraction = 1.0

batch_size = 2

data = {}
data['documents'] = []

# Utility functions
def metrics(preds, labels):
    acc = simple_accuracy(preds, labels)
    preds = [preds[i] for i in range(len(preds)) if labels[i][-1] == 0]
    labels = [labels[i] for i in range(len(labels)) if labels[i][-1] == 0]
    f1macro = precision_recall_fscore_support(y_true = labels, y_pred = preds, average = 'macro', beta = 1)[2]
    f1micro = precision_recall_fscore_support(y_true = labels, y_pred = preds, average = 'micro', beta = 1)[2]
    return {'acc': acc, 'f1_macro': f1macro, 'f1_micro': f1micro}

def preprocess(text):
    # Remove everything except: "'?!.,:a-zA-ZüöäÜÖÄèéà
    text = text.replace('\n', ' ')
    text = text.split(' ')
    text = [re.sub("[^\"'?!.,:a-zA-ZüöäÜÖÄèéà]+", "", t) for t in text]
    text = [t for t in text if len(t) > 2]
    text2 = []

    # Remove words that repeat more than once
    for i in range(len(text) - 1):
        remove = False
        if text[i] == text[i + 1]:
            remove = True
        if not remove:
            text2.append(text[i])
    text = []

    # Remove characters that repeat more than twice
    for t in text2:
        remove = False
        for i in range(len(t) - 2):
            if t[i] == t[i + 1] and t[i + 1] == t[i + 2]:
                remove = True
        if not remove:
            text.append(t)
    text = ' '.join(text)
    return text

def define_classes(sonstiges):
    classes = [c.replace(' ', '').replace(',', '') for c in unique_classes if c not in sonstiges]
    if len(sonstiges) > 0:
        classes.append('Sonstiges')
    print(classes)

    data = {}
    for c in classes:
        data[c] = []
    for index, row in df.iterrows():
        c = row['Class'].replace(' ', '').replace(',', '')
        if c in classes:
            data[c].append(str(row['file (Originalname)'])[:-4] + '.txt')
        else:
            data['Sonstiges'].append(str(row['file (Originalname)'])[:-4] + '.txt')

    for c in ['Prospektion', 'Exploration', 'Energierohstoffe', 'MineralischeRohstoffe', 'Bergbau', 'Naturgefahren', 'MessungenLaborresultate', 'Wissenschaft', 'Geophysik', 'Altlasten', 'Geothermie']:
        if os.path.exists(data_location2 + c):
            for f in list(os.walk(data_location2 + c))[0][2]:
                if f[-4:] == '.txt':
                    copyfile(data_location2 + c + '/' + f, data_location + 'Testmenge-Labeling/' + f)
                    if c in classes:
                        data[c].append(f)
                    else:
                        data['Sonstiges'].append(f)

    tmp = list(os.walk(data_location + 'Testmenge-Labeling'))[0][2]
    files = []
    data_reversed = {}
    for f in tmp:
        if f[-4:] == '.txt':
            files.append(f)
    for c in classes:
        for f in data[c]:
            if f in files:
                data_reversed[f] = c

    return data_reversed, classes, files

def format_data(file_name, skip, data_reversed, d, augment_factor = 0, predictions = None, atype = 0, size = 40, output_sonstiges = True):
    texts = []
    labels = []
    cnt = -1
    if os.path.exists(file_name):
        os.remove(file_name)
    with open(file_name, 'w', encoding = 'utf-8') as t:
        t.write('text\tlabel\n')
        for i, file in enumerate(d):
            if file in data_reversed:
                if skip(i, cnt, str(data_reversed[file])):
                    continue
                cnt += 1
                if predictions is not None and predictions[cnt]:
                    continue

                if file_name == 'train.tsv' and (data_reversed[file] != 'Sonstiges' or output_sonstiges):
                    if file.find('_') != -1:
                        idnr = file[:file.find('_')]
                    else:
                        idnr = file[:file.find('.')]

                    data['documents'].append({
                        'InfGeolNr': idnr,
                        'categoryProbabilities': {},
                        'predictedClass': '',
                        'annotatedClass': data_reversed[file]
                    })

                with open(data_location + 'Testmenge-Labeling/' + file, encoding = 'utf-8') as f:
                    text = preprocess(f.read())
                    t.write(text + '\t' + str(data_reversed[file]) + '\n')
                    texts.append(text)
                    labels.append(str(data_reversed[file]))

                    # data augmentation
                    if file_name == 'train.tsv' and augment_factor > 0:
                        if atype == 0 or atype == 2:
                            for _ in range(int(augment_factor)):
                                start = random.randint(0, int(len(text) * 0.5))
                                if len(text) - start < size:
                                    start = max(0, start - (size - len(text) - start))
                                length = random.randint(size, max(size + 1, len(text) - start))
                                texts.append(text[start:start + length])
                                labels.append(str(data_reversed[file]))
                                t.write(text[start:start + length] + '\t' + str(data_reversed[file]) + '\n')

                        if atype == 1 or atype == 2:
                            for _ in range(int(augment_factor)):
                                position = 0
                                tmp = ''
                                for _ in range(100):
                                    start = random.randint(position, position + size)
                                    if start >= len(text):
                                        break
                                    tmp += text[start:start + 10] + ' '
                                    position = start + 10
                                texts.append(tmp)
                                labels.append(str(data_reversed[file]))
                                t.write(tmp + '\t' + str(data_reversed[file]) + '\n')
            else:
                pass
    return texts, labels

# FARM initialization
register_metrics('metrics', metrics)
device, n_gpu = initialize_device_settings(use_cuda = True)

results = []
for s in [7]:
    results.append([])
    for aug in [1]:
        set_all_seeds(seed = s)
        np.random.seed(s)

        # Data Processor and Tokenizer: handles raw text and transforms them for training
        tokenizer = Tokenizer.load(pretrained_model_name_or_path = lang_model, do_lower_case = do_lower_case)

        df = pd.read_excel(data_location + 'Dok_Labeling_total.xlsx')
        unique_classes = df['Class'].unique()
        df.head()

        sonstiges = ['Prospektion', 'Exploration', 'Energierohstoffe', 'Mineralische Rohstoffe', 'Bergbau', 'Naturgefahren', 'Messungen, Laborresultate', 'Wissenschaft', 'Geophysik', 'Altlasten', 'Geologie allgemein',
                     'Geothermie']
        data_reversed, classes, files = define_classes(sonstiges)
        val_indices = np.random.randint(len(list(data_reversed)), size = 100)

        d = list(data_reversed)

        val_texts, val_labels = format_data('val.tsv', lambda i, cnt, label: i not in val_indices, data_reversed, d)

        # Step-1 model
        if not skip_first:
            texts, _ = format_data('train.tsv', lambda i, cnt, label: i in val_indices, data_reversed, d, augment_factor = 0, atype = 0, size = 79, output_sonstiges = False)
            if fraction < 1.0:
                with open('train.tsv', 'r', encoding = 'utf-8') as f:
                    lines = f.readlines()
                with open('train.tsv', 'w', encoding = 'utf-8') as f:
                    for i, line in enumerate(lines):
                        if i < len(texts) * fraction:
                            f.write(line)
            processor = TextClassificationProcessor(tokenizer = tokenizer, max_seq_len = 444, data_dir = Path("./"), label_list = classes, label_column_name = "label",
                                                    metric = 'metrics', quote_char = '"', multilabel = True, train_filename = "train.tsv", dev_filename = "val.tsv",
                                                    test_filename = None, dev_split = 0.0)

            # DataSilo: loads several datasets (train/dev/test), provides DataLoaders for them
            data_silo = DataSilo(processor = processor, batch_size = batch_size)

            language_model = LanguageModel.load(lang_model)
            layers = 4 * [866] + [len(classes)]
            prediction_head = MultiLabelTextClassificationHead(layer_dims = layers, num_labels = len(classes))  # , class_weights = data_silo.calculate_class_weights(task_name = "text_classification")

            # AdaptiveModel: consists of a pretrained language model as a basis and a prediction head on top
            model = AdaptiveModel(language_model = language_model, prediction_heads = [prediction_head], embeds_dropout_prob = 0.16589310697186702, lm_output_types = ["per_sequence"], device = device)

            # Optimizer
            model, optimizer, lr_schedule = initialize_optimizer(model = model, learning_rate = 3.8249088709627656e-05, device = device, n_batches = len(data_silo.loaders["train"]), n_epochs = n_epochs, grad_acc_steps = 14)

            # Trainer: trains and evaluates the model
            earlystopping = EarlyStopping(patience = 10, save_dir = 'model/step1/', metric = 'f1_micro', mode = "max")
            trainer = Trainer(model = model, optimizer = optimizer, data_silo = data_silo, epochs = n_epochs, n_gpu = n_gpu, lr_schedule = lr_schedule,
                              evaluate_every = len(data_silo.loaders["train"]) * 1, early_stopping = earlystopping, device = device, grad_acc_steps = 14)

            trainer.train()

            # Final evaluation
            evaluator = Evaluator(data_loader = data_silo.get_data_loader("dev"), tasks = data_silo.processor.tasks, device = device)
            model = AdaptiveModel.load('model/step1/', device)
            model.connect_heads_with_processor(processor.tasks)
            result = evaluator.eval(model, return_preds_and_labels = True)

            results[-1].append(result[0]['f1_micro'])
            print(results)

            processor = None
            data_silo = None
            prediction_head = None
            trainer = None

        # Step-2 model
        if not skip_second:
            set_all_seeds(seed = s)
            np.random.seed(s)
            if not skip_first and not independent:
                model = Inferencer.load('model_test/step1', gpu = True, task_type = 'text_classification', max_seq_len = 444, num_processes = 0, batch_size = batch_size)  #
                infered = model.inference_from_file('val.tsv')
                predictions = []
                for infer in infered:
                    predictions.extend([not p['label'] == "['Sonstiges']" for p in infer['predictions']])  # [not p['predictions'][0]['label'][0] == 'Sonstiges' for p in model.inference_from_file('train.tsv')]
                model = None
            else:
                predictions = [not label == 'Sonstiges' for label in val_labels]
            sonstiges = ['Prospektion', 'Exploration']  # , 'Geologie allgemein']#, 'Energierohstoffe', 'Mineralische Rohstoffe', 'Bergbau']
            data_reversed, classes2, files = define_classes(sonstiges)
            if skip_first or independent:
                classes2 = [c for c in classes2 if c not in ['Bohrung', 'Geotechnik', 'Hydrogeologie']]

            val_texts, val_labels = format_data('val.tsv', lambda i, cnt, label: i not in val_indices or label in ['Bohrung', 'Geotechnik', 'Hydrogeologie'], data_reversed, d, predictions)

            classes2 = [c for c in classes2 if c in val_labels]
            texts, labels = format_data('train.tsv', lambda i, cnt, label: i in val_indices or label in ['Bohrung', 'Geotechnik', 'Hydrogeologie'], data_reversed, d, augment_factor = 2, atype = 0, size = 40)  #

            if fraction < 1.0:
                with open('train.tsv', 'r', encoding = 'utf-8') as f:
                    lines = f.readlines()
                with open('train.tsv', 'w', encoding = 'utf-8') as f:
                    for i, line in enumerate(lines):
                        if i < len(texts) * fraction:
                            f.write(line)

            print(classes2)
            processor = TextClassificationProcessor(tokenizer = tokenizer, max_seq_len = 454, data_dir = Path("./"), label_list = classes2, label_column_name = "label",
                                                    metric = 'metrics', quote_char = '"', multilabel = True, train_filename = "train.tsv", dev_filename = "val.tsv",
                                                    test_filename = None, dev_split = 0.0)

            # DataSilo: loads several datasets (train/dev/test), provides DataLoaders for them
            data_silo = DataSilo(processor = processor, batch_size = batch_size)

            language_model = LanguageModel.load(lang_model)

            layers = 1 * [633] + [len(classes2)]
            prediction_head = MultiLabelTextClassificationHead(layer_dims = layers, num_labels = len(classes2), class_weights = data_silo.calculate_class_weights(task_name = "text_classification"))  # )

            # AdaptiveModel: consists of a pretrained language model as a basis and a prediction head on top
            model = AdaptiveModel(language_model = language_model, prediction_heads = [prediction_head], embeds_dropout_prob = 0.023408736698420776, lm_output_types = ["per_sequence"], device = device)

            # Optimizer
            model, optimizer, lr_schedule = initialize_optimizer(model = model, learning_rate = 1.4083407255166909e-05, device = device, n_batches = len(data_silo.loaders["train"]), n_epochs = n_epochs, grad_acc_steps = 45)

            # Trainer: trains and evaluates the model
            earlystopping = EarlyStopping(patience = 6, save_dir = 'model/step2/', metric = 'f1_micro', mode = "max")
            trainer = Trainer(model = model, optimizer = optimizer, data_silo = data_silo, epochs = n_epochs, n_gpu = n_gpu, lr_schedule = lr_schedule,
                              evaluate_every = len(data_silo.loaders["train"]) * 1, early_stopping = earlystopping, device = device, grad_acc_steps = 45)

            trainer.train()

            # Final evaluation
            evaluator = Evaluator(data_loader = data_silo.get_data_loader("dev"), tasks = data_silo.processor.tasks, device = device)
            model = AdaptiveModel.load('model/step2/', device)
            model.connect_heads_with_processor(processor.tasks)
            result2 = evaluator.eval(model, return_preds_and_labels = True)

            results[-1].append(result2[0]['f1_micro'])

            processor = None
            data_silo = None
            prediction_head = None
            trainer = None

        if not skip_first:
            print('F1-Score: ' + str(f1_score(y_true = np.argmax(result[0]['labels'], axis = 1), y_pred = np.argmax(result[0]['preds'], axis = 1), average = 'micro')))
            cmtx = pd.DataFrame(
                confusion_matrix(np.argmax(result[0]['labels'], axis = 1), np.argmax(result[0]['preds'], axis = 1)),
                index = [c[:8] for c in classes],
                columns = [c[:8] for c in classes]
            )
            print(cmtx)

        if not skip_second:
            print('F1-Score: ' + str(f1_score(y_true = np.argmax(result2[0]['labels'], axis = 1), y_pred = np.argmax(result2[0]['preds'], axis = 1), average = 'micro')))
            cmtx = pd.DataFrame(
                confusion_matrix(np.argmax(result2[0]['labels'], axis = 1), np.argmax(result2[0]['preds'], axis = 1)),
                index = [c[:7] for c in classes2],
                columns = [c[:7] for c in classes2]
            )
            print(cmtx)

with open('json/documents_annotated.json', 'w') as outfile:
    json.dump(data, outfile)