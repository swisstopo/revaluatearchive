## Document classification

- Installation: 

Linux Packages:

`apt-get install tesseract-ocr tesseract-ocr-fra tesseract-ocr-deu ocrmypdf libicu-dev poppler-utils ghostscript unpaper tmux python3-pip libgl1-mesa-glx`

From PyPi: 

`pip3 install ocrmypdf pdfCropMargins pdf2image farm xlrd==1.2.0 pandas`

- Requirements:

Python >= 3.6.0 (as requested by https://pypi.org/project/farm/)

FARM requirements: https://github.com/deepset-ai/FARM/blob/master/requirements.txt

## Scripts

### ocr_pipeline.py
This script is used to run OCR on documents. <br>
Input parameter: data folder with PDFs <br>
Output: ocr layer in PDF and a corresponding text file for each PDF

### training_pipeline.py
This script trains the classification models for step1 and step2. <br>
Input: labeled data <br>
Output: model files for step1 and step2. json containing all annotated labels for each InfGeolNr

### inference_pipeline.py 
This script is used to classify document. <br>
Input: data folder containing text files <br>
Output: json containing all infered labels for each InfGeolNr

### test.py
This script tests whether the ocr_pipeline and inference_pipeline is working correctly or not. <br>
Input: data folder with PDFs <br>
Output: test result