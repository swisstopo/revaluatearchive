import json
import subprocess

print('ocr')
cmd = ['python3', 'scripts/ocr_pipeline.py', 'data_test/']
proc = subprocess.run(cmd, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)

print('classification')
cmd = ['python3', 'scripts/inference_pipeline.py', 'data_test/']
proc = subprocess.run(cmd, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)


with open('json/documents_infered.json') as f:
  data = json.load(f)

cnt = 0
for doc in data['documents']:
    if doc['InfGeolNr'] == '25835' and doc['predictedClass'] == 'Wissenschaft':
        cnt += 1
    elif doc['InfGeolNr'] == '145' and doc['predictedClass'] == 'Bohrung':
        cnt += 1
    elif doc['InfGeolNr'] == '41537' and doc['predictedClass'] == 'Bergbau':
        cnt += 1

if cnt == 3:
    print('success')
else:
    print('did not classify all documents correctly')