import os
import numpy as np
import logging
import re
import warnings
import pandas as pd
import json
import argparse

from farm.infer import Inferencer
from farm.utils import MLFlowLogger, initialize_device_settings
from farm.utils import set_all_seeds
from farm.evaluation.metrics import simple_accuracy, register_metrics
from sklearn.metrics import precision_recall_fscore_support

warnings.simplefilter("ignore")
logging.disable(logging.CRITICAL)
pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 20)

batch_size = 2
data = {}
data['documents'] = []

# Utility functions
def parse_args():
    """Use argparse to parse command line arguments and pass it on to the main function."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'inferencedir',
        help = 'Directory to walk in order to find text documents to be classified')
    return parser.parse_args()

def metrics(preds, labels):
    acc = simple_accuracy(preds, labels)
    preds = [preds[i] for i in range(len(preds)) if labels[i][-1] == 0]
    labels = [labels[i] for i in range(len(labels)) if labels[i][-1] == 0]
    f1macro = precision_recall_fscore_support(y_true = labels, y_pred = preds, average = 'macro', beta = 1)[2]
    f1micro = precision_recall_fscore_support(y_true = labels, y_pred = preds, average = 'micro', beta = 1)[2]
    return {'acc': acc, 'f1_macro': f1macro, 'f1_micro': f1micro}

def preprocess(text):
    # Remove everything except: "'?!.,:a-zA-ZüöäÜÖÄèéà
    text = text.replace('\n', ' ')
    text = text.split(' ')
    text = [re.sub("[^\"'?!.,:a-zA-ZüöäÜÖÄèéà]+", "", t) for t in text]
    text = [t for t in text if len(t) > 2]
    text2 = []

    # Remove words that repeat more than once
    for i in range(len(text) - 1):
        remove = False
        if text[i] == text[i + 1]:
            remove = True
        if not remove:
            text2.append(text[i])
    text = []

    # Remove characters that repeat more than twice
    for t in text2:
        remove = False
        for i in range(len(t) - 2):
            if t[i] == t[i + 1] and t[i + 1] == t[i + 2]:
                remove = True
        if not remove:
            text.append(t)
    text = ' '.join(text)
    return text

def format_data(file_name, args, predictions, files, filenames):
    texts = []
    ids = []
    if os.path.exists(file_name):
        os.remove(file_name)
    with open(file_name, 'w', encoding = 'utf-8') as t:
        t.write('text\tlabel\n')
        i = 0
        for f in range(len(files)):
            if filenames[f][-4:] != '.txt':
                continue
            if predictions is not None and i not in predictions:
                continue
            if filenames[f].find('_') != -1:
                idnr = filenames[f][:filenames[f].find('_')]
            else:
                idnr = filenames[f][:filenames[f].find('.')]

            with open(files[f], encoding = 'utf-8') as f:
                text = preprocess(f.read())
                t.write(text + '\t' + '' + '\n')
                ids.append(idnr)
            i += 1
    return ids

# FARM initialization
register_metrics('metrics', metrics)
device, n_gpu = initialize_device_settings(use_cuda = True)

# class definitions for step1 and step2
classes = ['Bohrung', 'Geotechnik', 'Hydrogeologie', 'Sonstiges']
classes2 = ['Bergbau', 'Mineralische Rohstoffe', 'Geologieallgemein', 'Messungen, Laborresultate', 'Energierohstoffe', 'Geophysik', 'Geothermie', 'Naturgefahren', 'Altlasten', 'Wissenschaft', 'Sonstiges']

# random seed, only needed to replicate results
set_all_seeds(seed = 2)
np.random.seed(2)

# read in files
args = parse_args()
files = []
filenames = []
for dir_name, subdirs, file_list in os.walk(args.inferencedir):
    files.extend([dir_name + '/' + filename for filename in file_list])
    filenames.extend(file_list)
ids = format_data('infer.tsv', args, None, files, filenames)

predictions = []
probabilities = []

# Step-1 model
model = Inferencer.load('model/step1/', gpu = True, task_type = 'text_classification', max_seq_len = 444, num_processes = 0, batch_size = batch_size)  #
infered = model.inference_from_file('infer.tsv')
i = 0
for batch in infered:
    for infer in batch['predictions']:
        if infer['label'] == "['Sonstiges']":
            predictions.append(i)
            probabilities.append({classes[j]: str(infer['probability'][j]) for j in range(len(classes)) if classes[j] != 'Sonstiges'})
        else:
            data['documents'].append({
                'InfGeolNr': ids[i],
                'categoryProbabilities': {classes[j]: str(infer['probability'][j]) for j in range(len(classes))},
                'predictedClass': infer['label'][2:-2],
                'annotatedClass': ''
            })
        i += 1

# Step-2 model
ids = format_data('infer.tsv', args, predictions, files, filenames)

model = Inferencer.load('model/step2/', gpu = True, task_type = 'text_classification', max_seq_len = 454, num_processes = 0, batch_size = batch_size)  #
infered = model.inference_from_file('infer.tsv')

i = 0
for batch in infered:
    for infer in batch['predictions']:
        p = {classes2[j]: str(infer['probability'][j]) for j in range(len(classes2))}
        data['documents'].append({
            'InfGeolNr': ids[i],
            'categoryProbabilities': {**p, **probabilities[i]},
            'predictedClass': infer['label'][2:-2],
            'annotatedClass': ''
        })
        i += 1

with open('json/documents_infered.json', 'w') as outfile:
    json.dump(data, outfile)