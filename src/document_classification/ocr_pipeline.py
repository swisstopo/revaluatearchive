#!/usr/bin/env python3

from pdf2image import convert_from_path
import os
import subprocess
import argparse

priorities = {}
batch_size = 32
lngs = 'deu+fra+eng'

def parse_args():
    """Use argparse to parse command line arguments and pass it on to the main function."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pdfdir',
        help = 'Directory to walk in order to add text layer to PDFs within.')
    return parser.parse_args()

# Initialize the priority queue
def build_queue(files):
    global priorities

    priorities = {f: 0 for f in files}

# Returns "batch_size"-number of files with the highest priority
def get_batch(batch_size):
    global priorities

    if len(priorities) == 0:
        return None

    order = sorted(priorities.items(), key = lambda item: item[1])[::-1]
    batch = [k for k, v in order[:batch_size]]
    priorities = {k: v for k, v in order[batch_size:]}
    return batch

# Increase the priority of a file by "increment" amount
def update_priority(file, increment = 1):
    global priorities

    if file in priorities:
        priorities[file] += increment


def main(args: argparse.Namespace) -> None:
    files = []
    for dir_name, subdirs, file_list in os.walk(args.pdfdir):
        file_list = [file for file in file_list if str(file[:-4]) + '.txt' not in file_list and str(file[-4:]) == '.pdf' and not file[-12:] == '_cropped.pdf']
        files.extend([dir_name + '/' + filename for filename in file_list])
    build_queue(files)

    batch = get_batch(batch_size)
    while batch is not None:
        for nf, full_path in enumerate(batch):
            print(str(nf + 1) + ' / ' + str(len(batch)))
            print(full_path)
            full_path_cropped = full_path

            # Rescale the document if it is too small for the ocr process.
            n_pages = len(convert_from_path(full_path_cropped, 1))
            pages = convert_from_path(full_path_cropped, 10, first_page = 1, last_page = min(5, n_pages + 1))
            if sum([page.width < 20 for page in pages]) > 0:
                images = []
                for i in range(1, n_pages + 1):
                    page = convert_from_path(full_path_cropped, 10, first_page = i, last_page = i)[0]
                    if page.width < 20:
                        image = convert_from_path(full_path_cropped, 3000, first_page = i, last_page = i)[0]
                        (width, height) = (image.width * 2, image.height * 2)
                        images.append(image.resize((width, height)))
                    else:
                        images.append(convert_from_path(full_path_cropped, 300, first_page = i, last_page = i)[0])

                images[0].save(full_path_cropped, "PDF", resolution = 300, save_all = True, append_images = images[1:])

            # Start the tesseract ocr process
            cmd = ['ocrmypdf', '-l', lngs, '--force-ocr', '--clean', '-j1', '--remove-background', '--tesseract-pagesegmode', '1', full_path_cropped, full_path_cropped]

            proc = subprocess.run(
                cmd, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
            print(proc)
            if proc.returncode == 6:
                print("Skipped document because it already contained text")
            elif proc.returncode == 0:
                print("OCR complete")

            subprocess.run(['pdftotext', full_path_cropped, full_path[:-4] + '.txt'])

        batch = get_batch(batch_size)

if __name__ == '__main__':
    main(parse_args())
