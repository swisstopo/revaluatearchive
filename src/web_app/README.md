# Dokument Webapp

### Set Up 

1. Clone Git Repo:
2. Bilder hinzufügen:
Bilder müssen unter folgendem Pfad hinzugefügt werden:
`/swisstopowebapp/app/media/images`
3. Venv definieren unter "Python Interpreter"
![Clone Git Repo](documentation/venv.png)

4. Abhängigkeiten installieren (lokal ohne uWSGI):
`pip install -r requirements.txt`

### Aufbau der Webapp

##### App
Django Applikation unterteilt in Unterapplikationen. Folgende Files werden verwendet:
* **url.py:** Mappt eine URL einer Funktion zu
* **views.py:** Die effektive Python Funktion, welche ausgeführt wird
* **es.py:** Verbindung zu Elastic Search
* **models.py:** Wird nicht verwendet. Kann für DB-Models verwendet werden.
* **admin.py:** Wird nicht verwendet. Kann fürs Zusammenstellen der Admin-Seite verwendet werden.
* **templates/:** Beinhaltet HTML-Templates
* **static/:** Beinhaltet CSS und JS-Files sowie Bilder
* **templatetags/:** Beinhaltet Funktionen, welche von Templates aufgerufen werden zur Ausgabe von Daten
* **migrations/:** Wird nicht verwendet. Hier würden Änderungen der DB-Models gespeichert werden.

##### Proxy
Konfiguration des NGINX, welcher über Docker gestartet werden kann.

##### Scripts
Einstiegs-Script, welcher in Docker-Container ausgeführt wird, um Applikation zu starten.

#### Zugriff Docker Production
`docker context create gaia --docker host=ssh://dockermanager@gaia.hostings.swisstopo.cloud:22222`
`docker context use gaia`

#### URL
`gaia.hostings.swisstopo.cloud`

#### Docker commands
`docker service rm $(docker service ls -q)`
`docker exec -it ecd488be9f24 bash`
`docker container ls`
`docker cp e0314a6a86c9:/app/db.sqlite3 .`
`docker stack deploy -c docker-compose-deploy.yml prod`
`docker image build -t django-gaia:0.10 .`
`docker image ls`

