#!/usr/bin/env python
# coding: utf-8

import sys
sys.path.insert(1, '../../src')

import os
import xml.etree.ElementTree as ET

from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan

from pyproj import Transformer
from shapely.geometry.polygon import Polygon


import urllib3
urllib3.disable_warnings()


"""es = Elasticsearch(
        ['https://localhost:8443'],
        http_auth=("joshua-meier", "xxxxx"),
        timeout=30,
        verify_certs=False,
        ssl_show_warn=False,
        #sc"heme="",
    )"""

#print(es)

    
es = Elasticsearch(
             ['https://vpc-gaia-7naexopznyyq6yjzmndpoatdti.eu-central-1.es.amazonaws.com'],
              http_auth=("gaia-webapp", "ziu6deeth8aiSha-"),
        timeout=30,
        verify_certs=False,
        ssl_show_warn=False,
        #scheme="",
        port=8443
    )




# ### Mapping Löschen und neues Generieren. ACHTUNG: Alle existierenden Daten werden gelöscht!!

#es.indices.delete(index='swisstopo-documents', ignore=[400, 404])

# result = es.indices.create(index='swisstopo-documents', body={
#     'mappings': {
#         'properties': {
#             'title': {'type': 'text'},
#             'originalTitle': {'type': 'text'},
#             'originalId': {'type': 'text'},
#             'reportStructure': {'type': 'text' },
#             'availability': {'type': 'text' },
#             'auxiliary': {'type': 'text'},
#             'infogeol': {'type': 'long'},
#             'receiveDate': {'type': 'date', 'format': 'yyyy-MM-dd', 'ignore_malformed' : True},
#             'returnDate': {'type': 'date', 'format': 'yyyy-MM-dd', 'ignore_malformed' : True},
#             'creationDate': {'type': 'date', 'format': 'yyyy-MM-dd', 'ignore_malformed' : True},
#             'originalCreationDate': {'type': 'date', 'format': 'yyyyMMdd', 'ignore_malformed' : True},
#             'modificationDate': {'type': 'date', 'format': 'yyyy-MM-dd', 'ignore_malformed' : True},
#             'initiator': {'type': 'text'},
#             'supplier': {'type': 'text'},
#             'coordinates' : {'type': 'geo_shape', 'ignore_malformed' : True},
#             "locationInfo": {
#                 "properties": {
#                     "area": {"type": "float"},
#                     "coordinates_LV03": {"type": "long"},
#                     "type": {"type": "keyword"}
#                 }
#             },
#             "category": {'type': 'keyword'},
#             "categoryPredicted":  {"type": "keyword"},
#             "categoryProbability":  {"type": "long"},
#             "author.name":  {"type": "text"},
#             "author.code":  {"type": "text"},
#             "author.location":  {"type": "text"},
#             "requestCounter":  {"type": "long"}
#         }
#     }
# })



def get_text_from_tag(tree, tag):
    try:
        return tree.find(tag).text
    except AttributeError:
        return 'None'

transformer = Transformer.from_crs(21781, 4326, always_xy=True)
def get_geoshape_from_coordinates(coord_list):
    coordinates = [(int(x), int(y)) for _, _, y, x in coord_list]
    
    geoShape_dict = {}  
    location_info = {'area': 0}    
    
    if len(coord_list) == 1:
        geoShape_dict["type"] = "point"
        geoShape_dict["coordinates_LV03"] = coordinates[0]
        geoShape_dict["coordinates"] = transformer.transform(*coordinates[0])

    # If the last element points back to first element it is a polygon
    elif coord_list[0][0] == coord_list[-1][1]:
        geoShape_dict["type"] = "polygon"
        geoShape_dict["orientation"] = "clockwise"
        
        if len(coord_list) == 4:            
            coordinates = [coordinates[i] for i in [0,1,3,2]]     
        coordinates.append(coordinates[0])        
            
        geoShape_dict["coordinates"] = [[transformer.transform(*c) for c in coordinates]]
        location_info["area"] = Polygon(coordinates).area / 1000000 # m^2 -> km^2

    # If all elements point to themselves it is a multipoint
    elif all([x[0] == x[1] for x in coord_list]):
        geoShape_dict["type"] = "multipoint"
        geoShape_dict["coordinates"] = [transformer.transform(*c) for c in coordinates]
        
    

    # Else it must be a linestring
    else:
        geoShape_dict["type"] = "linestring"
        geoShape_dict["coordinates"] = [transformer.transform(*c) for c in coordinates]
      
    location_info["coordinates_LV03"] = coordinates  
    location_info["type"] = geoShape_dict["type"]
    return geoShape_dict, location_info

def get_geocode(tree):
    geocode_name = tree.tag.replace(ns, '')
    for subtree in tree:
        geocode_name += ' / ' + get_geocode(subtree)
    return geocode_name


# ### Kontakt Informationen in Dict laden
XML_PATH_AUT = "/notebook/InfoGeol-Contact-Extract"
with open(XML_PATH_AUT, encoding='ISO 8859-1', mode='r') as fp:
    root2 = ET.fromstring(fp.read())
    
ns = '{http://www.bwg.admin.ch/migeol/contact}'

contact_dict = {}

for child in root2:
    contact_string = ""
    id_ = child.get(ns+"object-id")
    code =  child.find(ns+'code').text
    name =  child.find(ns+'name').text
    
    contact_string = f"{id_}^:"
    if child.find(ns+'location') is not None:
        location = child.find(ns+'location').text
        
    contact_dict[id_] = {"code": code, "name": name, "location": location, "ID":id_}


# ### XML mit Metadaten Laden
XML_PATH = "/notebook/metadata2.xml"
with open(XML_PATH, encoding='ISO 8859-1', mode='r') as fp:
    root = ET.fromstring(fp.read())
    
ns = '{http://www.bwg.admin.ch/migeol/document}'


# ### Komplett Import vom gesamten XML-File


# i = 0
# for document in root.findall(ns+'document'):
#     document_dict = {}
#     header = document.find(ns+'header')
#     abstract = document.find(ns+'abstract')
#     geology_info = abstract.find(ns+'geology-info')
#
#     document_dict['infogeol'] = int(header.get(ns+'sgd-number'))
#     document_dict['creationDate'] = header.get(ns+'creation-date', '0001-01-01')
#     document_dict['originalCreationDate'] = header.get(ns+'original-document-creation-date', '00010101')
#     document_dict['modificationDate'] = header.get(ns+'modification-date')
#     document_dict['returnDate'] = header.get(ns+'return-date')
#     document_dict['receiveDate'] = header.get(ns+'receive-date')
#
#
#     document_dict['originalId'] = get_text_from_tag(header, ns+'original-document-id')
#     document_dict['originalTitle'] = get_text_from_tag(header, ns+'orig-title')
#     document_dict['title'] = get_text_from_tag(header, ns+'title')
#     document_dict['supplier'] = get_text_from_tag(header, ns+'supplier')
#     document_dict['initiator'] = get_text_from_tag(header, ns+'initiator')
#     document_dict['auxiliary'] = get_text_from_tag(header, ns+'auxiliary-information')
#     report_structure = abstract.find(ns+'report-structure')
#     report_structure_string = ""
#
#     if(report_structure is not None):
#         for item in [list(c.attrib.values()) for c in report_structure.findall(ns+'amount')]:
#             report_structure_string += f'{item[1]} {item[0]}, '
#
#
#     document_dict['reportStructure'] = report_structure_string[:-2]
#
#     availability = header.find(ns+'availability')
#
#     availability_string = ""
#     for item in [list(c.attrib.values()) for c in availability.findall(ns+'restriction')]:
#         index = 2
#         if len(item) != 3:
#             index -= 1
#
#         if item[index] == "view":
#             availability_string += "Ansicht (view): "
#         elif item[index] == "copy":
#             availability_string += "Kopie (copy): "
#
#         if item[index - 1] == "free":
#             availability_string += "frei (free). "
#         elif item[index - 1] == "on-request":
#             availability_string += "auf Anfrage (on request). "
#
#     document_dict['availability'] = availability_string
#
#     document_dict["requestCounter"] = 0
#     # get coordinates
#     coord_list = [list(c.attrib.values()) for c in geology_info.findall(ns+'coordinate')]
#     document_dict['coordinates'], document_dict['locationInfo'] = get_geoshape_from_coordinates(coord_list)
#
#     # get geology codes
#     geology_codes = set()
#     for geology_code_tree in geology_info.findall(ns+'geology-code'):
#         for geology_code_subtree in geology_code_tree:
#             subtree_name = get_geocode(geology_code_subtree)
#             if not 'undefiniert' in subtree_name:
#                 geology_codes.add(subtree_name)
#
#     document_dict['category'] = get_text_from_tag(header, ns+'document-category')
#
#     if document.find(ns+'header').find(ns + 'author-codes') is not None:
#         aut = document.find(ns+'header').find(ns + 'author-codes').get(ns + 'id')
#
#     document_dict["author.name"] = contact_dict[aut]["name"]
#     document_dict["author.code"] = contact_dict[aut]["code"]
#     document_dict["author.location"] = contact_dict[aut]["location"]
#
#     es.index('swisstopo-documents', id=document_dict['infogeol'], body=document_dict)
#


# ### Update Beispiel

# update
for document in root.findall(ns+'document'):
    document_dict = {}
    header = document.find(ns+'header')
    infogeol = document.find(ns+'header').get(ns+'sgd-number')

    document_dict['category'] = get_text_from_tag(header, ns+'document-category')
    result = es.update('swisstopo-documents', id=infogeol, body={'doc': document_dict})



