from django.contrib.auth import views
from django.urls import path
from . import views


urlpatterns = [
    path('logged_out/', views.logged_out, name='logged_out'),
]
