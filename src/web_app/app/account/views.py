from django.shortcuts import render


# Create your views here.
def logged_out(request):
    return render(request, 'registration/logged_out2.html')
