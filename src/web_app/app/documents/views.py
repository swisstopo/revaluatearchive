from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from elasticsearch import helpers

from . import es
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from app.decorators.decorators import ajax_required

import csv
from django.http import HttpResponse

document_categories = [
    'Exploration', 'Energierohstoffe', 'Prospektion', 'Bergbau', 'MineralischeRohstoffe', 'Naturgefahren',
    'Geothermie', 'MessungenLaborresultate', 'Wissenschaft', 'Geophysik', 'Altlasten', 'Geologieallgemein',
    'Hydrogeologie', 'Bohrung', 'Geotechnik', 'unknown']

object_categories = [
    'Bohrprofil', 'Karte', 'Geotechnisches Profil', 'Geologisches Profil', 'Stratigraphisches Profil',
    'Seismisches Profil', 'Bohr-Log', 'Tabelle', 'Grafik', 'Foto',
]

es = es.get_es_connection()


def import_json_from_inference_detail(request):
    return render(request, 'documents/import_json_from_inference_detail.html', {"categories": document_categories,
                                                                                "page_name": "inference_import"
                                                                                })


def mlexport(request):
    query = {"query": {
        "bool": {
            "must_not": [{"terms": {"category": ["unknown"]}}],
        }
    }}

    result = es.search(index="swisstopo-documents",
                       body=query, size=10000)
    return JsonResponse(
        [{"infoGeolNr": x["_source"]["infogeol"], "category": x["_source"]["category"], "documentUrl": "TBD"}
         for x in result["hits"]["hits"]]
        , safe=False)


@login_required
def dashboard_view(request):
    filtered_categories = []
    filtered_object_categories = []
    infogeol_list = []

    if request.GET.getlist("category[]") is not None:
        filtered_categories = request.GET.getlist("category[]")

    if request.GET.getlist("objectCategory[]") is not None and len(request.GET.getlist("objectCategory[]")) > 0:
        filtered_object_categories = request.GET.getlist("objectCategory[]")

        annotations = es.search(index="swisstopo-annotations", body={"query": {
            "bool": {
                "filter": [{"terms": {"category": filtered_object_categories}}],
            }
        }}, size=500)

        predictions = es.search(index="swisstopo-predictions", body={"query": {
            "bool": {
                "filter": [{"terms": {"category": filtered_object_categories}}],
            }
        }}, size=5000)

        unique_infogeols = set(
            [x["_source"]["infogeol"] for x in annotations['hits']['hits'] if "infogeol" in x["_source"] and x["_source"]["infogeol"].isnumeric()] + [
                int(x["_source"]["infogeol"]) for x in predictions['hits']['hits'] if
                x["_source"]["infogeol"].isnumeric()])
    else:
        unique_infogeols = None

    if request.GET.get("infogeol_list") is not None and request.GET.get("infogeol_list") is not "":
        if "-" in request.GET.get("infogeol_list"):
            infogeol_list = list(range(int(request.GET.get("infogeol_list").split("-")[0]),
                                       int(request.GET.get("infogeol_list").split("-")[1])))
        else:
            infogeol_list = request.GET.get("infogeol_list").split(",")

        if unique_infogeols is not None:
            infogeol_list = set(infogeol_list).intersection(unique_infogeols)
            infogeol_list = list(infogeol_list)
            if (len(infogeol_list) == 0):
                infogeol_list.append(99999999)
    elif unique_infogeols is not None:
        infogeol_list = list(unique_infogeols)

    query = {'sort': [{'infogeol': 'asc'}],
             "query": {
                 "bool": {
                     "must": [],
                     "filter": [],
                 }
                 #   , 'sort': [{'infogeol': 'asc'}]
             }}

    if request.GET.get("top_left_lat") is not None and request.GET.get("top_left_lat") is not "":
        query["query"]["bool"]["filter"].append(
            {"geo_bounding_box":
                {
                    "coordinates": {
                        "top_left": {
                            "lon": float(request.GET.get("top_left_lon")),
                            "lat": float(request.GET.get("top_left_lat"))

                        },
                        "bottom_right": {
                            "lon": float(request.GET.get("bottom_right_lon")),
                            "lat": float(request.GET.get("bottom_right_lat"))

                        }
                    }
                }
            })

    if request.GET.get("start_date") is not None and request.GET.get("start_date") is not "":
        query["query"]["bool"]["filter"].append({"range": {
            "originalCreationDate": {
                "gte": request.GET.get("start_date").replace("-", ""),
                "lte": request.GET.get("end_date").replace("-", "")
            }
        }})

    if request.GET.get("full_text") is not None and request.GET.get("full_text") is not "":
        query["query"]["bool"]["must"].append({"multi_match": {"query": request.GET.get("full_text"),
                                                               "fields": ["initiator", "auxiliary",
                                                                          "reportStructure",
                                                                          "supplier", "title", "originalId",
                                                                          "Originaltitel",
                                                                          "author.name", "availability"]}})

    if request.GET.get("request_counter") is not None and request.GET.get("request_counter") is not "":
        query["query"]["bool"]["filter"].append({"range": {
            "requestCounter": {
                "gte": request.GET.get("request_counter"),
                "lte": 99999999
            }
        }})

    if len(filtered_categories) > 0:
        query["query"]["bool"]["filter"].append({"terms": {"category": filtered_categories}})
    else:
        query["query"]["bool"]["filter"].append({"terms": {"category": document_categories}})

    if len(infogeol_list) > 0:
        query["query"]["bool"]["filter"].append({"terms": {"infogeol": infogeol_list}})

    if False and request.GET.get("supplier") is not None and request.GET.get("supplier") is not "":
        query["query"]["bool"]["must"].append({"match": {"supplier": request.GET.get("supplier")}})

    if request.GET.get("initiator") is not None and request.GET.get("initiator") is not "":
        query["query"]["bool"]["must"].append({"match": {"initiator": ".*" + request.GET.get("initiator") + ".*"}})

    if request.GET.get("author") is not None and request.GET.get("author") is not "":
        query["query"]["bool"]["must"].append({"match": {"author.name": ".*" + request.GET.get("author") + ".*"}})

    if request.GET.get("original_id") is not None and request.GET.get("original_id") is not "":
        query["query"]["bool"]["must"].append({"match": {"originalId": ".*" + request.GET.get("original_id") + ".*"}})

    if request.GET.get("export_json") is not None:
        maxDocuments = 10000
    else:
        maxDocuments = 800

    if request.GET.get("_search") is not None:

        search_results = es.search(index="swisstopo-documents",

                                   body=query, size=maxDocuments)

        foundDocuments = [x["_source"]["infogeol"] for x in search_results["hits"]["hits"]]

        if request.GET.get("export_json") is not None:
            return JsonResponse(search_results["hits"]["hits"], safe=False)

        if request.GET.get("export_csv") is not None:

            response = HttpResponse(
                content_type='text/csv',
                headers={'Content-Disposition': 'attachment; filename="export.csv"'},
            )

            labels = ["infogeol", "creationDate", "originalCreationDate", "modificationDate", "returnDate",
                      "receiveDate", "originalId", "originalTitle", "title", "supplier", "initiator", "auxiliary",
                      "reportStructure", "availability", "coordinates", "locationInfo", "category", "requestCounter",
                      "author.name", "author.code", "author.location"]

            writer = csv.writer(response)
            writer.writerow(labels)

            for result in search_results["hits"]["hits"]:
                arr = []
                for l in labels:
                    arr.append(result["_source"][l])
                writer.writerow(arr)

            return response

        annotations = es.search(index="swisstopo-annotations", body={"query": {
            "bool": {
                "filter": [{"terms": {"infogeol": foundDocuments}}],
            }
        }}, size=500)

        predictions = es.search(index="swisstopo-predictions", body={"query": {
            "bool": {
                "filter": [{"terms": {"infogeol": foundDocuments}}],
            }
        }}, size=500)

        if request.GET.get("_search") is not None and len(infogeol_list) > 0:

            def gendata(inputs):
                for input in inputs:
                    yield {
                        '_op_type': 'update',
                        '_index': 'swisstopo-documents',
                        '_type': '_doc',
                        '_id': input["_source"]["infogeol"],
                        'doc': {'requestCounter': input["_source"]["requestCounter"] + 1}
                    }

            helpers.bulk(es, gendata(search_results["hits"]["hits"]))
    else:
        search_results = {"hits": {"hits": []}}
        annotations = {"hits": {"hits": []}}
        predictions = {"hits": {"hits": []}}

    return render(request, 'documents/dashboard.html', {"documents": search_results["hits"]["hits"],
                                                        "infogeol_list": request.GET.get("infogeol_list"),
                                                        "supplier": request.GET.get("supplier"),
                                                        "author": request.GET.get("author"),
                                                        "initiator": request.GET.get("initiator"),
                                                        "start_date": request.GET.get("start_date"),
                                                        "end_date": request.GET.get("end_date"),
                                                        "original_id": request.GET.get("original_id"),
                                                        "full_text": request.GET.get("full_text"),
                                                        "request_counter": request.GET.get("request_counter"),
                                                        "document_categories": document_categories,
                                                        "object_categories": object_categories,
                                                        "filtered_categories": filtered_categories,
                                                        "filtered_object_categories": filtered_object_categories,
                                                        "annotations": annotations["hits"]["hits"],
                                                        "predictions": predictions["hits"]["hits"],
                                                        "page_name": "dashboard",
                                                        "number_of_results": len(search_results["hits"]["hits"])
                                                        }
                  )


def document_detail_view(request, document_id):
    document = es.get(index='swisstopo-documents', id=document_id)

    pages = es.search(index="swisstopo-pages",
                      body={'query': {'term': {'infogeol': document_id}}, 'sort': [{'page': 'asc'}]}, size=100)
    return render(request, 'documents/document_detail.html', {"document": document["_source"],
                                                              "pages": pages["hits"],
                                                              "categories": document_categories})


def page_detail_view(request, index):
    file = es.get(index='swisstopo-pages', id=index)
    annotations = es.search(index="swisstopo-annotations", body={'query': {'term': {'page_id': index}}}, size=200)
    predictions = es.search(index="swisstopo-predictions", body={'query': {'term': {'page_id': index}}}, size=200)
    return render(request, 'documents/page_detail.html', {'file': file,
                                                          'annotations': annotations["hits"]["hits"],
                                                          'predictions': predictions["hits"]["hits"],
                                                          'categories': object_categories})


@ajax_required
@api_view(["PUT"])
def update_category(request, index):
    document_dict = {}
    document_dict['category'] = request.data["category"]
    result = es.update('swisstopo-documents', id=index, body={'doc': document_dict})
    return Response({"status": "ok"}, status=status.HTTP_200_OK)


@ajax_required
@api_view(["PUT"])
def update_inference_category(request, index):
    document_dict = {}
    document_dict['categoryPredicted'] = request.data["category"]
    document_dict['categoryProbability'] = request.data["probability"]
    result = es.update('swisstopo-documents', id=index, body={'doc': document_dict})
    return Response({"status": "ok"}, status=status.HTTP_200_OK)


@ajax_required
@api_view(["POST"])
def add_annotation(request, index):
    result = es.index(index='swisstopo-annotations', body={
        'page_id': index,
        'bbox': [request.data['x1'], request.data['y1'], request.data['x2'], request.data['y2']],
        'category': request.data['category'],
        'origin': 'tool',
    })
    return Response({"id": result['_id']}, status=status.HTTP_200_OK)


@ajax_required
@api_view(["PUT"])
def update_annotation(request, index, annotation_id):
    es.update(index='swisstopo-annotations', id=annotation_id, body={
        'doc': {
            'bbox': [request.data['x1'], request.data['y1'], request.data['x2'], request.data['y2']],
            'category': request.data['category'],
        }
    })
    return Response({"status": "ok"}, status=status.HTTP_200_OK)


@ajax_required
@api_view(["DELETE"])
def delete_annotation(request, dict_index, annotation_id):
    es.delete(index='swisstopo-annotations', id=annotation_id)
    return Response({"status": "ok"}, status=status.HTTP_200_OK)
