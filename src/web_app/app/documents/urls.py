from django.urls import path
from . import views

urlpatterns = [

    path('', views.dashboard_view, name='dashboard'),
    path('importJson/', views.import_json_from_inference_detail, name='jsonTest'),
    path('mlexport/', views.mlexport, name='jsonTest'),
    path('detail/<document_id>/', views.document_detail_view, name='document_detail'),
    path('category/<index>/', views.update_category, name='update_category'),
    path('inferenceCategory/<index>/', views.update_inference_category, name='update_category'),
    path('page/<index>/', views.page_detail_view, name='page_detail'),
    path('page/annotate/<index>/', views.add_annotation, name='add_annotation'),
    path('page/annotate/<index>/<annotation_id>/', views.update_annotation, name='update_annotation'),
    path('page/annotate/delete/<dict_index>/<annotation_id>/', views.delete_annotation, name='delete_annotation'),
]
