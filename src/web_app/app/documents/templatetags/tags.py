import json

from django import template

register = template.Library()


@register.filter
def value(dict, key):
    #print(key)
    if key in dict:
        return dict[key]
    else:
        return ""


@register.filter
def modulo(num, val):
    return num % val


@register.filter
def to_json(val):
    return json.dumps(val)


@register.filter
def empty_if_none(val):
    if val is None:
        return ""
    else:
        return val


@register.filter
def format_creation_data(val):
    return val[0:4] + "-" + val[4:6] + "-" + val[6:8]
