from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
 #   path('accounts/login/', admin.site.urls),
    path('login/', admin.site.urls),
    url(r'^documents/', include('documents.urls')),
    url('', include('documents.urls')),
#    url(r'^account/', include('account.urls')),
    path('accounts/', include('django.contrib.auth.urls')),  # new
    path('accounts/', include('account.urls')),  # new

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
