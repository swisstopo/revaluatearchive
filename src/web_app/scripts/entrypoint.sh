#!/bin/sh

set -e

echo "Run Migrations"
python manage.py migrate

python manage.py collectstatic --noinput
uwsgi --socket :8000 --master --enable-threads --module app.wsgi